from node import Node
from collections import Counter
import copy
import pandas as pd
import numpy as np
from export import export_text
from copy import copy

def entropy(target_col):
    target_values, counts = np.unique(target_col,return_counts=True)
    entropy = np.sum([(-counts[i]/sum(counts))*np.log2(counts[i]/sum(counts)) for i in range(len(target_values))])
    return entropy

def information_gain(examples, target_attribute, selected_attribute, cont=False, split=None):
    priorEntropy = entropy(examples[target_attribute])
    if cont:
        splitted_data1 = examples.loc[examples[selected_attribute]<=split]
        splitted_data2 = examples.loc[examples[selected_attribute]>split]
        entropySum = entropy(splitted_data1[target_attribute])*len(splitted_data1)/len(examples) + entropy(splitted_data2[target_attribute])*len(splitted_data2)/len(examples)
    else:
        selected_attribute_values, counts = np.unique(examples[selected_attribute],return_counts=True)
        entropySum = np.sum([entropy(examples.where(examples[selected_attribute]==selected_attribute_values[i]).dropna()[target_attribute])*counts[i]/np.sum(counts) for i in range(len(counts))])
    informationGain = priorEntropy - entropySum
    return informationGain

#menentukan apakah atribut kontinu
def is_kontinu(examples, selected_attribute, threshold):
        selected_attribute_values = np.unique(examples[selected_attribute])
        #jika kontinu
        if(len(selected_attribute_values) > threshold):
            return True

#mencari kandidat split
def split_data(examples, target_attribute, selected_attribute):
        sorted_data = examples.sort_values(by= [selected_attribute])
        sorted_data = sorted_data.reset_index(drop=True)
        target_val_before = sorted_data[target_attribute][0]
        data_before = sorted_data[selected_attribute][0]
        splits = []
        for index,data in sorted_data.iterrows():
            if data[target_attribute] != target_val_before:
                splits.append(data_before)
                target_val_before = data[target_attribute]
                data_before = data[selected_attribute]
        split_candidate = np.unique(splits)
        return split_candidate

#menentukan pembatas terbaik
def best_split(examples, target_attribute, selected_attribute, split_candidate):
        maxGain = -999
        selected_split = None
        for split in split_candidate:
            gain = information_gain(examples,target_attribute,selected_attribute,cont=True,split=split)
            if gain > maxGain:
                maxGain = gain
                selected_split = split
        return selected_split

def attribute_label(examples):
    target_values, counts = np.unique(examples, return_counts = True)
    return target_values[np.argmax(counts)]

def get_continue_value(border, attribute_value):
    if attribute_value > border:
        return 'more'
    else:
        return 'less'

def split_in_information(examples, selected_attribute, cont=False, split=None):
    if cont:
        splitted_data1 = examples.loc[examples[selected_attribute]<=split]
        splitted_data2 = examples.loc[examples[selected_attribute]>split]
        splitEntropy = - len(splitted_data1)/len(examples)*np.log2(len(splitted_data1)/len(examples)) - len(splitted_data2)/len(examples)*np.log2(len(splitted_data1)/len(examples)) 
    else:
        selected_attribute_values, counts = np.unique(examples[selected_attribute],return_counts=True)
        count = np.sum(counts)
        splitEntropy = np.sum([(-counts[i]/count*np.log2(counts[i]/count)) for i in range(len(selected_attribute_values))])
    return splitEntropy

def gain_ratio(examples, target_attribute, selected_attribute, cont=False, split=None):
    gainRatio = information_gain(examples, target_attribute, selected_attribute)/split_in_information(examples, selected_attribute, cont, split)
    return gainRatio

def missing_value_handling(instances):
    header = []
    for i in instances.columns:
        header.append(str(i))

    # Membuat list berisi value dari atribut
    attributes = instances.values.tolist()

  # Membuat list berisi list yang mengandung missing value
  # Membuat list berisi index tempat missing value berada
    missing_value_list = []
    index = []
    for attribute in attributes:
      for i in range (len(attribute)):
        if attribute[i] == '?':
          missing_value_list.append(attribute)
          index.append(i)
    missing_value = list(set(tuple(x) for x in missing_value_list))
    index = list(set(index))

    labels = []
    for arr in missing_value_list:
      labels.append(arr[len(arr)-1])
    labels = list(set(labels))

    for label in labels: #yes/no
      class_list = []
      for attribute in attributes:
        if label in attribute:
          class_list.append(attribute)
    
    # Menghitung maks occurence di tiap kolom, dengan key adalah kolom dan value adalah most element
    dict = {}
    for i in index:
      dict[i] = mostElement(attributes,i)
    for i in range(len(attributes)):
      for j in range (len(attribute)):
        if (attributes[i][j] =='?'):
          attributes[i][j] = dict.get(j)
    return pd.DataFrame(attributes,columns = header)

def mostElement(attributes,index):
    element = []
    for attribute in attributes:
      for i in range(len(attribute)):
        if i == index:
          element.append(attribute[i])
          count = Counter(element)
    try :
      most_common = count.most_common(mode(element))
    except:
      most_common = count.most_common(1)
    
    return(most_common[0][0])

def separate_data(examples):
    training_set = examples.sample(frac = 0.2, random_state = 0)
    test_set = examples.drop(training_set.index)
    return test_set, training_set

class MyID3:
    def __init__(self):
        self._tree = None
    
    def best_attribute(self, examples, target_attribute, features):
        selectedAttribute = None
        maxGain = -9999
        for feature in features:
            gain = information_gain(examples,target_attribute,feature)
            # print(gain)
            if maxGain < gain:
                maxGain = gain
                selectedAttribute = feature
        return selectedAttribute
    
    def fit(self, examples,target_attribute,feature, originalExamples=None):
        self._tree = self.fitID3(examples,target_attribute,feature,originalExamples)
        return self._tree

    def fitID3(self, examples, target_attribute, feature, originalExamples = None):
        tree = Node()
        targets = np.unique(examples[target_attribute])
        # print(targets)
        if len(targets) <=1 :
            tree.label=targets[0]
        elif len(feature) ==0:
            return tree
        else:
            bestAttribute = self.best_attribute(examples,target_attribute,feature)
            tree.attribute = bestAttribute
            tree.label = attribute_label(examples)
            # print(bestAttribute)
            new_feature = []
            for elmt in feature:
                if elmt != bestAttribute:
                    new_feature.append(elmt)
            # print(bestAttribute)
            tree.attribute_values = np.unique(examples[bestAttribute])
            # print(bestAttributeValues)
            for bestAttributeValue in tree.attribute_values:
                # print(bestAttributeValue)
                # print(examples.where(examples[bestAttribute]==bestAttributeValue).dropna())
                subtree = self.fit(examples.where(examples[bestAttribute]==bestAttributeValue).dropna(), target_attribute, new_feature, examples)
                subtree.parrent_attribute = bestAttribute
                subtree.parrent_attribute_value = bestAttributeValue
                tree.children[bestAttributeValue] = subtree
        return tree
    
    def writeFile(self,filename):
        self._tree.writeFile(filename)
    
    def readFile(self,filename):
        # self._tree = Node()
        self._tree.readFile(filename)

class MyC45:
    def __init__(self):
        self._tree = None
        self.continue_attributes = []
    
    def predict(self, dec_tree, record):
        if len(dec_tree.children) ==0:
            # print(dec_tree.attribute, dec_tree.label)
            return dec_tree.label
        else:
            # print(dec_tree.attribute, dec_tree.label)
            if dec_tree.is_pruned :
                
                return dec_tree.label
            else:
                if dec_tree.continue_attribute :
                    return self.predict(dec_tree.children[get_continue_value(dec_tree.border,record[dec_tree.attribute])], record)
                else:
                    # print(dec_tree.children[record[dec_tree.attribute]].label)
                    return self.predict(dec_tree.children[record[dec_tree.attribute]], record)
                
    def accuracy(self, datatest,target_attribute):
        error = 0
        total = 0
        for index,row in datatest.iterrows():
            total +=1
            if row[target_attribute] != self.predict(self._tree,row):
                # print(row[target_attribute], predict(dec_tree,row))
                error +=1
                # print(index)
        return error/total

    def best_attribute(self, examples, target_attribute, features):
        selectedAttribute = None
        splitValue = None
        border = None
        maxGain = -9999
        # print(self.continue_attributes)
        for feature in features:
            if feature in self.continue_attributes:
                border = best_split(examples, target_attribute, feature, split_data(examples, target_attribute, feature))
                gain = gain_ratio(examples,target_attribute,feature,True,border)
            else:
                gain = gain_ratio(examples,target_attribute,feature)
                border = None
            # print(gain)
            if maxGain < gain:
                maxGain = gain
                selectedAttribute = feature
                splitValue = border
        return selectedAttribute, splitValue

    def prune(self, datatest, target_attribute, dec_tree):
        # If leaf
        if len(dec_tree.children) == 0: 
            return
        
        for attribute_value, children in dec_tree.children.items():
            self.prune(datatest, target_attribute, children)
        # print (dec_tree.attribute, dec_tree.parrent_attribute, dec_tree.parrent_attribute_value)
        error_before_prune = self.accuracy(datatest,target_attribute)
        dec_tree.is_pruned = True
        error_after_prune = self.accuracy(datatest,target_attribute)
        # print(error_before_prune, error_after_prune)
        if error_after_prune > error_before_prune:
            dec_tree.is_pruned = False
        return

    def fitc45(self, examples, target_attribute, feature, originalExamples = None):
        tree = Node()
        targets = np.unique(examples[target_attribute])
        # print(targets)
        if len(feature) ==0:
            return
        if len(examples) == 0:
            return 
        if len(targets) <=1 :
            tree.label=targets[0]
        elif len(feature) ==0:
            return tree
        else:
            bestAttribute, border = self.best_attribute(examples,target_attribute,feature)
            # print(bestAttribute)
            tree.attribute = bestAttribute
            tree.label = attribute_label(examples[target_attribute])
            # print(bestAttribute)
            new_feature = []
            for elmt in feature:
                if elmt != bestAttribute:
                    new_feature.append(elmt)
            # print(bestAttribute)
            # print(new_feature)
            if border:
                tree.continue_attribute = True
                tree.border = border
                tree.attribute_values = []
                if len(examples.where(examples[bestAttribute]>tree.border).dropna())>=1:
                    tree.attribute_values.append('more')
                if len(examples.where(examples[bestAttribute]<=tree.border).dropna())>=1:
                    tree.attribute_values.append('less')
                for bestAttributeValue in tree.attribute_values:
                    if bestAttributeValue == 'more':
                        # print(examples.where(examples[bestAttribute] > tree.border).dropna())
                        subtree = self.fitc45(examples.where(examples[bestAttribute] > tree.border).dropna(), target_attribute, new_feature, examples)
                    else:
                        # print(examples.where(examples[bestAttribute] <= tree.border).dropna())
                        subtree = self.fitc45(examples.where(examples[bestAttribute] <= tree.border).dropna(), target_attribute, new_feature, examples)
                    if subtree:
                        subtree.parrent_attribute = bestAttribute
                        subtree.parrent_attribute_value = bestAttributeValue
                        tree.children[bestAttributeValue] = subtree
            else:
                tree.attribute_values = np.unique(examples[bestAttribute])
            # print(bestAttributeValues)
                for bestAttributeValue in tree.attribute_values:
                    # print(bestAttributeValue)
                    # print(examples.where(examples[bestAttribute]==bestAttributeValue).dropna())
                    subtree = self.fitc45(examples.where(examples[bestAttribute]==bestAttributeValue).dropna(), target_attribute, new_feature, examples)
                    if subtree:
                        subtree.parrent_attribute = bestAttribute
                        subtree.parrent_attribute_value = bestAttributeValue
                        tree.children[bestAttributeValue] = subtree
        return tree

    def fit(self, examples, target_attribute, feature, originalExamples = None):
        # print(self.continue_attributes)
        for header in examples.head():
            if is_kontinu(examples,header,10):
                self.continue_attributes.append(header)
        
        examples = missing_value_handling(examples)
        
        # print(examples)
        # print(_tree.attribute)
        # print(export_text_id3(_tree))
        # print(examples, target_attribute, feature)
        test_data, training_data = separate_data(examples)
        # print(training_data)
        # print(len(training_data), len(test_data))
        self._tree = self.fitc45(training_data, target_attribute, feature)
        self.prune(test_data,target_attribute, self._tree)
        return self._tree